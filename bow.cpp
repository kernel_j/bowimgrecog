#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/xfeatures2d/nonfree.hpp"
#include "opencv2/core/types.hpp"
#include "opencv2/ml.hpp"

#define CLUSTERS 200
#define RETRY 1
#define FILES 20

using namespace cv;

void    extractFeatures(Mat *src)
{
    Mat                         img;
    Mat                         descriptor;
    Ptr<FeatureDetector>        detector;
    Ptr<DescriptorExtractor>    extractor;
    std::vector<KeyPoint>       keypoint;
    std::string                 filenum;
    int                         i;

    detector = xfeatures2d::SIFT::create();
    extractor = xfeatures2d::SIFT::create();
    i = 0;
    while (i < FILES)
    {
        std::stringstream out;
        out << i;
        filenum = out.str();
        img = imread("../trainData/positive/cat" + filenum + ".jpg", IMREAD_GRAYSCALE);
        detector->detect(img, keypoint);
        extractor->compute(img, keypoint, descriptor);
        (*src).push_back(descriptor);
        i++;
    }
}

void    buildVocab(Mat *src, Mat *vocab)
{
    int i;
    TermCriteria tc(cv::TermCriteria::MAX_ITER + 
            cv::TermCriteria::EPS,
            10,
            0.001);
    BOWKMeansTrainer bowTrainer(CLUSTERS,
            tc,
            RETRY,
            KMEANS_PP_CENTERS);
    
    bowTrainer.add(*src);
    *vocab = bowTrainer.cluster();
}

void    getVocab(void)
{
    Mat extracted;
    Mat vocab;
    
    extractFeatures(&extracted);
    buildVocab(&extracted, &vocab);
    FileStorage fs("vocab.yml", FileStorage::WRITE);
    fs << "Vocabulary" << vocab;
    fs.release();
    std::cout << "write done" << std::endl;
}

void    getDescriptor(void)
{
    Mat dictionary;

    FileStorage fs("vocab.yml", FileStorage::READ);
    fs["Vocabulary"] >> dictionary;
    fs.release();

    Ptr<DescriptorMatcher> matcher(new FlannBasedMatcher);
    Ptr<FeatureDetector> detector;
    Ptr<DescriptorExtractor> extractor;

    detector = xfeatures2d::SIFT::create();
    extractor = xfeatures2d::SIFT::create();

    BOWImgDescriptorExtractor bowDE(extractor, matcher);
    bowDE.setVocabulary(dictionary);

    Mat                     img;
    Mat                     bowDescriptor;
    Mat                     trainData(0, CLUSTERS, CV_32FC1);
    Mat                     label(0, 1, CV_32SC1);
    std::vector<KeyPoint>   keypoints;
    std::string             filenum;
    int                     i;

    i = 0;
    while (i < FILES) 
    {
        std::stringstream out;
        out << i;
        filenum = out.str();
        img = imread("../trainData/positive/cat" + filenum + ".jpg", IMREAD_GRAYSCALE);
        detector->detect(img, keypoints);
        bowDE.compute(img, keypoints, bowDescriptor);
        trainData.push_back(bowDescriptor);
        label.push_back(1);
        i++;
    }
    i = 0;
    while (i < FILES) 
    {
        std::stringstream out;
        out << i;
        filenum = out.str();
        img = imread("../trainData/negative/neg" + filenum + ".jpg", IMREAD_GRAYSCALE);
        detector->detect(img, keypoints);
        bowDE.compute(img, keypoints, bowDescriptor);
        trainData.push_back(bowDescriptor);
        label.push_back(0);
        i++;
    }

    std::cout << "start training" << std::endl;
    Ptr<ml::SVM> svm = ml::SVM::create();
    svm->setType(ml::SVM::C_SVC);
    svm->setKernel(ml::SVM::LINEAR);
    svm->setTermCriteria(TermCriteria(cv::TermCriteria::MAX_ITER, 10, 0.001));
    svm->setGamma(3);
    svm->setDegree(3);
    
    std::cout << "train" << std::endl;
    svm->train(trainData, ml::ROW_SAMPLE, label);

    svm->save("svm.yml");
    std::cout << "finish writing to file" << std::endl;
}

void    svmExample(void)
{
    int     labels[10] = {0, 0, 1, 1, 0, 1, 1, 1, 1, 0 };
    Mat     labelsMat(10, 1, CV_32SC1, labels);

    float   train[10][2] = {{100, 10}, {150, 10}, {600, 200}, {600, 10}, {10, 100}, {455, 10}, {345, 255}, {10, 501}, {401, 255}, {30, 150}};
    Mat     trainMat(10, 2, CV_32FC1, train);
    
    Ptr<ml::SVM> svm = ml::SVM::create();
    svm->setType(ml::SVM::C_SVC);
    svm->setKernel(ml::SVM::LINEAR);
    svm->setTermCriteria(TermCriteria(cv::TermCriteria::MAX_ITER, 100, 1e-6));
    svm->setGamma(3);
    svm->setDegree(3);
    svm->train(trainMat, ml::ROW_SAMPLE, labelsMat);

    float   testData[2] = {400, 200};
    Mat     testMat(2, 1, CV_32FC1, testData);
    testMat = testMat.reshape(1, 1);

    float   predicts = svm->predict(testMat);
    std::cout << "Predicted label: " << predicts << std::endl;

}

void    predict(void)
{
    Mat dictionary;

    FileStorage fs("vocab.yml", FileStorage::READ);
    fs["Vocabulary"] >> dictionary;
    fs.release();

    Ptr<DescriptorMatcher> matcher(new FlannBasedMatcher);
    Ptr<FeatureDetector> detector;
    Ptr<DescriptorExtractor> extractor;
    std::vector<KeyPoint> keypoints;

    detector = xfeatures2d::SIFT::create();
    extractor = xfeatures2d::SIFT::create();

    BOWImgDescriptorExtractor bowDE(extractor, matcher);
    bowDE.setVocabulary(dictionary);
    
    Mat img;
    Mat bowDescriptor;
    Mat res;
    
    img = imread("../trainData/test4.jpg", IMREAD_GRAYSCALE);
    detector->detect(img, keypoints);
    bowDE.compute(img, keypoints, bowDescriptor);
    res.push_back(bowDescriptor);

    Ptr<ml::SVM> svm = Algorithm::load<ml::SVM>("svm.yml");
    std::cout << svm->predict(res) << std::endl;
}

int     main(void)
{
    //getVocab();
    //getDescriptor();
    //svmExample();
    predict();

    return (0);
}
